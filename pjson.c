#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "cjson.h"
#include "pjson.h"

typedef struct {
	kvec_t(pjson_element_t*) elements;
	char *key;
} pjson_data_t;

pjson_element_t *pjson_create_element(cjson_event_t event, const char *begin, const char *end) {
	pjson_element_t *element = calloc(1, sizeof(pjson_element_t));

	switch (event) {
		case CJSON_EVENT_OBEGIN:
			element->type = PJSON_TYPE_OBJECT;
			element->object = kh_init(pjson_map);
			break;
		case CJSON_EVENT_ABEGIN:
			element->type = PJSON_TYPE_ARRAY;
			element->array = calloc(1, sizeof(pjson_vec)); 
			break;
		case CJSON_EVENT_STRING:
			element->type = PJSON_TYPE_STRING;
			element->string = strndup(begin, end - begin);
			break;
		case CJSON_EVENT_NUMBER:
			element->type = PJSON_TYPE_NUMBER;
			element->number = strtod(begin, NULL);
			break;
		case CJSON_EVENT_BOOLEAN:
			element->type = PJSON_TYPE_BOOLEAN;
			element->boolean = (strncmp(begin, "true", sizeof("true") - 1) == 0);
			break;
		case CJSON_EVENT_NULL:
			element->type = PJSON_TYPE_NULL;
			break;
	}
	return element;
}

static void pjson_object_set(pjson_element_t *parent, const char *key, pjson_element_t *value) {
	int ret;
	khiter_t k = kh_put(pjson_map, parent->object, key, &ret);
	kh_value(parent->object, k) = value;
}

static void pjson_array_add(pjson_element_t *parent, pjson_element_t *value) {
	kv_push(pjson_element_t*, *parent->array, value);
}

static void pjson_add_element(pjson_data_t *data, pjson_element_t *element) {
	size_t n_elements = kv_size(data->elements);
	if (n_elements > 0) {
		pjson_element_t *parent = kv_A(data->elements, n_elements - 1);
		if (parent->type == PJSON_TYPE_OBJECT) {
			pjson_object_set(parent, data->key, element);
			data->key = NULL;
		}
		else if (parent->type == PJSON_TYPE_ARRAY)
			pjson_array_add(parent, element);
	}

	if (n_elements == 0 || element->type == PJSON_TYPE_OBJECT || element->type == PJSON_TYPE_ARRAY)
		kv_push(pjson_element_t*, data->elements, element);
}

void pjson_create(cjson_event_t event, const char *begin, const char *end, void *pdata) {
	pjson_data_t *data = (pjson_data_t*)pdata;

	switch (event) {
		case CJSON_EVENT_OBEGIN:
		case CJSON_EVENT_ABEGIN: {
			pjson_element_t *element = pjson_create_element(event, begin, end);
			pjson_add_element(data, element);
			break;
		}
		case CJSON_EVENT_OEND:
		case CJSON_EVENT_AEND:
			kv_pop(data->elements);
			break;
		case CJSON_EVENT_STRING: {
			size_t n_elements = kv_size(data->elements);
			if (n_elements > 0 && kv_A(data->elements, n_elements - 1)->type == PJSON_TYPE_OBJECT && !data->key)
				data->key = strndup(begin, end - begin);
			else {
				pjson_element_t *element = pjson_create_element(event, begin, end);
				pjson_add_element(data, element);
			}
			break;
		}
		case CJSON_EVENT_NUMBER:
		case CJSON_EVENT_BOOLEAN:
		case CJSON_EVENT_NULL: {
			pjson_element_t *element = pjson_create_element(event, begin, end);
			pjson_add_element(data, element);
			break;
	       }
	}
}

pjson_element_t *pjson_parse(const char *s) {
	pjson_data_t data = {0};
	if (cjson_parse(s, pjson_create, &data) == -1)
		return NULL;

	pjson_element_t *json = kv_A(data.elements, 0);
	kv_destroy(data.elements);
	return json;
}

void pjson_free(pjson_element_t *json) {
}

static pjson_element_t *pjson_key(pjson_element_t *element, const char *key) {
	if (element->type != PJSON_TYPE_OBJECT)
		return NULL;
	khiter_t k = kh_get(pjson_map, element->object, key);
	if (k == kh_end(element->object))
		return NULL;
	return kh_value(element->object, k);
}

static pjson_element_t *pjson_i(pjson_element_t *element, size_t i) {
	if (element->type != PJSON_TYPE_ARRAY)
		return NULL;
	if (i < 0 || i > kv_size(*element->array))
		return NULL;
	return kv_A(*element->array, i);
}

pjson_element_t *pjson_get(pjson_element_t *json, const char *path) {
	const char *p = path;
	const char *path_end = path + strlen(path);

	pjson_element_t *element = json;

	while (*p) {
		const char *end = strchr(p, '.');
		if (!end)
			end = path_end;
		if (isdigit(*p))
			element = pjson_i(element, strtol(p, NULL, 10));
		else {
			char *key = strndup(p, end - p);
			element = pjson_key(element, key);
			free(key);
		}
		if (!element)
			return NULL;
		p = end;
		if (*p == '.')
			p++;
	}

	return element;
}
