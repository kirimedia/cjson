#ifndef PJSON_H
#define PSSON_H

#include <stdbool.h>

#include "khash.h"
#include "kvec.h"

typedef enum {
	PJSON_TYPE_OBJECT,
	PJSON_TYPE_ARRAY,
	PJSON_TYPE_STRING,
	PJSON_TYPE_NUMBER,
	PJSON_TYPE_BOOLEAN,
	PJSON_TYPE_NULL,
} pjson_type_t;

struct pjson_element_s;
typedef struct pjson_element_s pjson_element_t;

KHASH_MAP_INIT_STR(pjson_map, pjson_element_t*);
typedef khash_t(pjson_map) pjson_map;
typedef kvec_t(pjson_element_t*) pjson_vec;

typedef struct pjson_element_s {
	pjson_type_t type;
	union {
		pjson_map *object;
		pjson_vec *array;
		char *string;
		double number;
		bool boolean;
	};
} pjson_element_t;

pjson_element_t *pjson_parse(const char *s);
void pjson_free(pjson_element_t *json);
pjson_element_t *pjson_get(pjson_element_t *json, const char *path);

#endif
