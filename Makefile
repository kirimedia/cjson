all: parser

%.o: %.c
	gcc -g -o $@ -c $^

parser: parser.o pjson.o cjson.o
	gcc -o $@ $^

clean:
	rm -f *.o parser

.PHONY: all, clean
