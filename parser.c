#include <stdio.h>

#include "pjson.h"

int main(int argc, char *argv[]) {
	if (argc != 3) {
		fprintf(stderr, "Usage: %s json path\n", argv[0]);
		return -1;
	}

	pjson_element_t *json = pjson_parse(argv[1]);

	if (!json)
		return -1;

	pjson_element_t *element = pjson_get(json, argv[2]);
	if (!element) {
		printf("not found\n");
		return 0;
	}

	switch (element->type) {
		case PJSON_TYPE_OBJECT:
			printf("object\n");
			break;
		case PJSON_TYPE_ARRAY:
			printf("array\n");
			break;
		case PJSON_TYPE_STRING:
			printf("%s\n", element->string);
			break;
		case PJSON_TYPE_NUMBER:
			printf("%f\n", element->number);
			break;
		case PJSON_TYPE_BOOLEAN:
			printf("%s\n", element->boolean ? "true" : "false");
			break;
		case PJSON_TYPE_NULL:
			printf("null\n");
			break;
	}

	return 0;
}
