#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "cjson.h"

typedef enum {
	L_OBEGIN,
	L_OEND,
	L_ABEGIN,
	L_AEND,
	L_COLON,
	L_COMMA,
	L_STRING,
	L_NUMBER,
	L_BOOL,
	L_NULL,
	L_COUNT,
	L_ERROR,
} lexem_t;

typedef enum {
	S_BEGIN,
	S_OBJECT,
	S_COLON,
	S_OVALUE,
	S_OCOMMA,
	S_KEY,
	S_ARRAY,
	S_ACOMMA,
	S_AVALUE,
	S_COUNT,
	S_END,
	S_ERR,
} state_t;

typedef int (*action_t)(const char *s, const char **end, cjson_callback_t callback, void *data);

typedef struct {
	state_t state;
	action_t action;
} rule_t;

static int get_json(const char *s, const char **end, cjson_callback_t callback, void *data);

static int obegin(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_OBEGIN, s, *end, data); return 0; }
static int oend(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_OEND, s, *end, data); return 0; }
static int abegin(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_ABEGIN, s, *end, data); return 0; }
static int aend(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_AEND, s, *end, data); return 0; }
static int string(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_STRING, s + 1, *end - 1, data); return 0; }
static int number(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_NUMBER, s, *end, data); return 0; }
static int boolean(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_BOOLEAN, s, *end, data); return 0; }
static int null(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_NULL, s, *end, data); return 0; }


static rule_t syntax[S_COUNT][L_COUNT] = {
//		L_OBEGIN, 		L_OEND, 	L_ABEGIN, 		L_AEND, 	L_COLON, 	L_COMMA, 	L_STRING, 		L_NUMBER, 	L_BOOL, 	L_NULL
/*S_BEGIN*/	{{S_OBJECT, obegin},	{S_ERR, NULL},	{S_ARRAY, abegin},	{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL},	{S_END, string},	{S_END, number},{S_END, boolean},{S_END, null}},
/*S_OBJECT*/	{{S_ERR, NULL},		{S_END, oend},	{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL},	{S_COLON, string},	{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL}},
/*S_COLON*/	{{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},		{S_ERR, NULL},	{S_OVALUE, NULL},{S_ERR, NULL},	{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL}},
/*S_OVALUE*/	{{S_OCOMMA, get_json},	{S_ERR, NULL},	{S_OCOMMA, get_json},	{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL},	{S_OCOMMA, get_json},	{S_OCOMMA, get_json},{S_OCOMMA, get_json},{S_OCOMMA, get_json}},
/*S_OCOMMA*/	{{S_ERR, NULL},		{S_END, oend},	{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},	{S_KEY, NULL},	{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL}},
/*S_KEY*/	{{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL},	{S_COLON, string},	{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL}},
/*S_ARRAY*/	{{S_ACOMMA, get_json},	{S_ERR, NULL},	{S_ACOMMA, get_json},	{S_END, aend},	{S_ERR, NULL},	{S_ERR, NULL},	{S_ACOMMA, get_json},	{S_ACOMMA, get_json},{S_ACOMMA, get_json},{S_ACOMMA, get_json}},
/*S_ACOMMA*/	{{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},		{S_END, aend},	{S_ERR, NULL},	{S_AVALUE, NULL},{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL}},
/*S_AVALUE*/	{{S_ACOMMA, get_json},	{S_ERR, NULL},	{S_ACOMMA, get_json},	{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL},	{S_ACOMMA, get_json},	{S_ACOMMA, get_json},{S_ACOMMA, get_json},{S_ACOMMA, get_json}},
};

static lexem_t get_string(const char *s, const char **end) {
	const char *p = s + 1;
	while (*p != '\0' && *p != '"') {
		if (*p == '\\') {
			char n = *(p + 1);
			if (n == '"' || n == '\\' || n == 'b' || n == 'f' || n == 'n' || n == 'r' || n =='t')
				p++;
			else
				return L_ERROR;
		}
		p++;
	}
	if (*p == '\0')
		return L_ERROR;
	*end = p + 1;
	return L_STRING;
}

static lexem_t get_value(const char *s, const char **end) {
	lexem_t lexem;
	const char *p = s;
	if (strncmp(p, "true", sizeof("true") - 1) == 0) {
		p += sizeof("true") - 1;
		lexem = L_BOOL;
	}
	else if (strncmp(p, "false", sizeof("false") - 1) == 0) {
		p += sizeof("false") - 1;
		lexem = L_BOOL;
	}
	else if (strncmp(p, "null", sizeof("null") - 1) == 0) {
		p += sizeof("null") - 1;
		lexem = L_NULL;
	}
	else {
		char *e;
		strtod(p, &e);
		if (p == e)
			return L_ERROR;
		p = e;
		lexem = L_NUMBER;
	}
	*end = p;
	return lexem;
}

static lexem_t get_lexem(const char *s, const char **end) {
	*end = s + 1;
	switch (*s) {
		case '{': return L_OBEGIN;
		case '}': return L_OEND;
		case '[': return L_ABEGIN;
		case ']': return L_AEND;
		case ':': return L_COLON;
		case ',': return L_COMMA;
		case '"': return get_string(s, end);
		default: return get_value(s, end);
	}
}

static int get_json(const char *s, const char **end, cjson_callback_t callback, void *data) {
	state_t state = S_BEGIN;
	while (*s) {
		while (isspace(*s))
			s++;
		lexem_t lexem = get_lexem(s, end);
		if (lexem == L_ERROR)
			return -1;
		rule_t rule = syntax[state][lexem];
		if (rule.state == S_ERR)
			return -1;
		if (rule.action != NULL) {
			if (rule.action(s, end, callback, data) == -1)
				return -1;
		}
		if (rule.state == S_END)
			return 0;
		state = rule.state;
		s = *end;
	}
	return -1;
}

int cjson_parse(const char *s, cjson_callback_t callback, void *data) {
	if (!s)
		return -1;
	const char *end;
	if (get_json(s, &end, callback, data) == -1)
		return -1;
	while (isspace(*end))
		*end++;
	if (*end != '\0')
		return -1;
	return 0;
}
